<?php
declare (strict_types = 1);

namespace app\controller;

use app\BaseController;
use app\model\ParamModel;
use app\model\CertificationModel;
use app\validate\CertificationValidate;
use app\validate\CertificationAddFormValidate;
use app\validate\CertificationEditFormValidate;
use app\validate\AttestationOriginalAddValidate;
use think\exception\ValidateException;

class Certification extends BaseController {

    /**
     * index 产品碳足迹认证管理列表
     * 
     * @return void
     */
    public function index() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：组织名称、产品名称、认证类型、状态、创建时间
        $filters = [
            'filter_organization' => isset($_GET['filterOrganization']) ? $_GET['filterOrganization'] : '',
            'filter_product_name' => isset($_GET['filterProductName']) ? $_GET['filterProductName'] : '',
            'filter_attestation_type' => isset($_GET['filterAttestationType']) ? $_GET['filterAttestationType'] : '',
            'filter_status' => isset($_GET['filterStatus']) ? $_GET['filterStatus'] : '',
            'filter_time_start' => isset($_GET['filterTimeStart']) ? $_GET['filterTimeStart'] : '',
            'filter_time_end' => isset($_GET['filterTimeEnd']) ? $_GET['filterTimeEnd'] : '',
        ];

        $list = CertificationModel::getAttestations($page_size, $page_index, $filters);

        $status_map = ParamModel::STATUS_MAP;
        $auth_map = ParamModel::AUTH_MAP;
        $list_new = [];
        foreach ($list['data'] as $k => $v) {

            if (isset($status_map[$v['status']])) {
                $v['status_name'] = $status_map[$v['status']];
            } else {
                $v['status_name'] = '';
            }

            if (isset($auth_map[$v['attestation_type']])) {
                $v['attestation_type_name'] = $auth_map[$v['attestation_type']];
            } else {
                $v['attestation_type_name'] = '';
            }

            $list_new[$k] = $v;
        }

        $data['code'] = 200;
        $data['data']['list'] = $list_new;
        $data['data']['status_select_map'] = ParamModel::STATUS_SELECT_MAP;
        $data['data']['auth_select_map'] = ParamModel::AUTH_SELECT_MAP;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * info 查看认证信息详情
     *
     * @return void
     */
    public function info() {

        /** 流程名称 */
        /**
         * 1. 只显示title的
         *  待线上核查
         *  待预约现场核查
         *  待现场核查
         *  待补充材料
         *  已完成认证，待派发证书
         *  
         * 2. 显示title username create_time
         *  提交认证申请
         *  已通过线上核查
         *  返回补充材料
         *  未通过线上核查
         * 
         * 3. 显示 title username create_time verify_name verify_mobile verify_start_time verify_end_time retinue content
         *  已预约现场核查
         * 
         * 4. 显示 title username create_time photo_files count(photo_files)
         *  现场核查已通过
         * 
         * 5. 显示 title username create_time certificate_files report_files
         *  证书已派发
         */

        $process_one = ['待线上核查', '待预约现场核查', '待现场核查', '待补充材料', '已完成认证，待派发证书'];
        $process_two = ['提交认证申请', '已通过线上核查'];
        $id = $_GET['id'];
        $list = CertificationModel::getInfoById($id);
        foreach ($list['process'] as $key => &$value) {
            $arr_new = [];
            if (in_array($value['title'], $process_one)) {
                $arr_new['id'] = $value['id'];
                $arr_new['title'] = $value['title'];
            } elseif (in_array($value['title'], $process_two)) {
                $arr_new['id'] = $value['id'];
                $arr_new['title'] = $value['title'];
                $arr_new['username'] = $value['username'];
                $arr_new['create_time'] = $value['create_time'];
            } elseif ($value['title'] == '未通过线上核查') {
                $arr_new['id'] = $value['id'];
                $arr_new['title'] = $value['title'];
                $arr_new['username'] = $value['username'];
                $arr_new['create_time'] = $value['create_time'];
                $arr_new['reject_cause'] = $value['reject_cause'];
            } elseif ($value['title'] == '退回补充材料') {
                $arr_new['id'] = $value['id'];
                $arr_new['title'] = $value['title'];
                $arr_new['username'] = $value['username'];
                $arr_new['create_time'] = $value['create_time'];
                $arr_new['statement'] = $value['statement'];
            } elseif ($value['title'] == '已预约现场核查') {
                $arr_new['id'] = $value['id'];
                $arr_new['title'] = $value['title'];
                $arr_new['username'] = $value['username'];
                $arr_new['verify_name'] = $value['verify_name'];
                $arr_new['verify_mobile'] = $value['verify_mobile'];
                $arr_new['verify_start_time'] = $value['verify_start_time'];
                $arr_new['verify_end_time'] = $value['verify_end_time'];
                $arr_new['retinue'] = $value['retinue'];
                $arr_new['content'] = $value['content'];
                $arr_new['verify_plan_file'][0] = CertificationModel::getCalculateFiles($value['verify_plan_file']);
                $arr_new['create_time'] = $value['create_time'];
            } elseif ($value['title'] == '现场核查已通过') {
                $arr_new['id'] = $value['id'];
                $arr_new['title'] = $value['title'];
                $arr_new['username'] = $value['username'];
                $arr_new['photo_count'] = $value['photo_count'];
                $arr_new['photo_files'] = $value['photo_files'];
                $arr_new['big_pic_list'] = $value['big_pic_list'];
                $arr_new['create_time'] = $value['create_time'];
            } elseif ($value['title'] == '证书已派发') {
                $arr_new['id'] = $value['id'];
                $arr_new['title'] = $value['title'];
                $arr_new['username'] = $value['username'];
                $arr_new['certificate_files'] = $value['certificate_files'];
                $arr_new['report_files'] = $value['report_files'];
                $arr_new['create_time'] = $value['create_time'];
            }

            $value = $arr_new;
        }

        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * datainfo 查看认证数据及材料详情
     *
     * @return void
     */
    public function datainfo() {
        $id = $_GET['id'];
        $list = CertificationModel::getdata($id);
        $data['code'] = 200;
        $data['data'] = $list;

        return json($data);
    }

    /**
     * batchCompar 批量对比
     *
     * @return void
     */
    public function batchCompar() {
        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data['compar'] = $params_payload_arr['compar'];
            if (empty($data['compar'])) {
                return json(['code' => 201, 'message' => "请批量选择数据"]);
            }
            $data['userid'] = request()->userInfo['userid'];

            $batch = CertificationModel::batchCompar($data);
            if ($batch) {
                return json(['code'=>200, 'message'=>"操作成功"]);
            } else{
                return json(['code'=>404, 'message'=>"操作成功"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"请求方式错误"]);
        }
    }

    /**
     * Right 数据错误/数据正确
     *
     * @return void
     */
    public function Right() {
        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data['userid'] = request()->userInfo['userid'];

            $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
                return json(['code' => 201, 'message' => "参数id错误"]);
            }

            $data['err_status'] = isset($params_payload_arr['err_status']) ? $params_payload_arr['err_status'] : '';
            if ($data['err_status'] == '' || $data['err_status'] == null || $data['err_status'] == 0) {
                return json(['code'=>201, 'message'=>"参数status错误"]);
            }

            $data['err_msg'] = isset($params_payload_arr['err_msg']) ? $params_payload_arr['err_msg'] : '';
            // 数据正确（2），数据错误（3）
            if (!in_array($data['err_status'], [ParamModel::ERR_STATUS_YES, ParamModel::ERR_STATUS_NO])) {
                return json(['code'=>201, 'message'=>"参数err_status错误"]);
            }

            if ($data['err_status'] == ParamModel::ERR_STATUS_YES) {
                $data['err_msg'] = '';
            }

            $update = CertificationModel::Right($data);
            if ($update) {
                return json(['code'=>200, 'message'=>"操作成功"]);
            } else {
                return json(['code'=>404, 'message'=>"操作失败"]);
            }
        } else {
            return json(['code'=>404, 'message'=>"请求方式错误"]);
        }
    }

    /**
     * onlineVerify 线上核查
     *
     * @return void
     */
    public function onlineVerify() {
        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $userid = request()->userInfo['userid'];
            $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            $data['reject_cause'] = isset($params_payload_arr['reject_cause']) ? $params_payload_arr['reject_cause'] : '';
            if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
                return json(['code'=>201, 'message'=>"参数id错误"]);
            }

            $data['status'] = isset($params_payload_arr['status']) ? $params_payload_arr['status'] : '';
            if ($data['status'] == '' || $data['status'] == null || $data['status'] == 0) {
                return json(['code'=>201, 'message'=>"参数status错误"]);
            }

            // 通过-待现场核查（4），驳回-已驳回（5）
            if (!in_array($data['status'], [ParamModel::STATUS_TO_CHECK_ONSITE, ParamModel::STATUS_REJECTED])) {
                return json(['code'=>201, 'message'=>"参数status错误"]);
            }

            $original_data = CertificationModel::getOriginalById($data['id'])->toArray();
            // 获取该认证的所有表单，通过线上核查需要所有表单都为 判断过数据正确/错误 err_status 1-未判断数据正确错误 2-数据正确 3-数据错误
            foreach ($original_data as $key => $value) {
                if ($value['err_status'] == ParamModel::ERR_STATUS_UNAUDIT && $data['status'] == ParamModel::STATUS_TO_CHECK_ONSITE) {
                    return json(['code'=>200, 'data'=>['message'=>"请在【数据及材料】中完成数据比对"]]);
                } elseif ($value['err_status'] == ParamModel::ERR_STATUS_NO && $data['status'] == ParamModel::STATUS_TO_CHECK_ONSITE) {
                    return json(['code'=>200, 'data'=>['message'=>"存在数据比对错误无法通过核查"]]);
                }
            }

            // 驳回原因
            if ($data['status'] == ParamModel::STATUS_REJECTED) {
                if ($data['reject_cause'] == '') {
                    return json(['code'=>201, 'message'=>"请输入驳回原因"]);
                } else {
                    if (strlen($data['reject_cause']) > 300)
                    return json(['code'=>201, 'message'=>"超出字数限制"]);
                }
            }

            $result = CertificationModel::onlineVerify($data['id'], $data['status'], $data['reject_cause'], $userid);
            if ($result && $data['status'] == ParamModel::STATUS_TO_CHECK_ONSITE) {
                return json(['code'=>200, 'message'=>"已通过线上核查"]);
            } elseif ($result && $data['status'] == ParamModel::STATUS_REJECTED) {
                return json(['code'=>200, 'message'=>"已驳回线上核查"]);
            } else {
                return json(['code'=>404, 'message'=>"操作失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"操作失败"]);
        }
    }

    /**
     * onSiteAppointmentVerify 预约现场核查
     *
     * @return void
     */
    public function onSiteAppointmentVerify() {
        if (request()->isPost() && $this->validateForm($type = 1) === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data['userid'] = request()->userInfo['userid'];
            $data['status'] = ParamModel::STATUS_ONSITE_CHECKING;
            $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
                return json(['code'=>201, 'message'=>"参数id错误"]);
            }

            $data['verify_name'] = $params_payload_arr['verify_name'];
            $data['verify_mobile'] = $params_payload_arr['verify_mobile'];
            $data['retinue'] = $params_payload_arr['retinue'];
            $data['content'] = $params_payload_arr['content'];
            $data['verify_plan_file'] = $params_payload_arr['verify_plan_file'];
            $data['verify_start_time'] = $params_payload_arr['verify_start_time'];
            $data['verify_end_time'] = $params_payload_arr['verify_end_time'];//return json($data);

            $result = CertificationModel::onSiteAppointmentVerify($data);
            if ($result) {
                return json(['code'=>200, 'message'=>"成功预约现场核查"]);
            } else {
                return json(['code'=>404, 'message'=>"预约失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm($type = 1)]);
        }
    }

    /**
     * onSiteVerify 现场核查
     *
     * @return void
     */
    public function onSiteVerify() {
        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data['userid'] = request()->userInfo['userid'];
            $data['statement'] = isset($params_payload_arr['statement']) ? $params_payload_arr['statement'] : '';
            $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
                return json(['code'=>201, 'message'=>"参数id错误"]);
            }

            $data['status'] = isset($params_payload_arr['status']) ? $params_payload_arr['status'] : '';
            if ($data['status'] == '' || $data['status'] == null || $data['status'] == 0) {
                return json(['code'=>201, 'message'=>"参数status错误"]);
            }

            // 通过-认证完成（7），驳回-退回补充材料（8）
            if (!in_array($data['status'], [ParamModel::STATUS_COMPLETED, ParamModel::STATUS_TO_SUPPLEMENTED])) {
                return json(['code'=>201, 'message'=>"参数status错误"]);
            }

            $original_data = CertificationModel::getOriginalById($data['id'])->toArray();
            // 获取该认证的所有表单，通过线上核查需要所有表单都为 判断过数据正确/错误 err_status 1-未判断数据正确错误 2-数据正确 3-数据错误
            foreach ($original_data as $key => $value) {
                if ($value['err_status'] == ParamModel::ERR_STATUS_UNAUDIT && $data['status'] == ParamModel::STATUS_COMPLETED) {
                    return json(['code'=>201, 'message'=>"请在【数据及材料】中完成数据比对"]);
                } elseif ($value['err_status'] == ParamModel::ERR_STATUS_NO && $data['status'] == ParamModel::STATUS_COMPLETED) {
                    return json(['code'=>201, 'message'=>"存在数据比对错误无法通过核查"]);
                }
            }

            // 补充说明
            if ($data['status'] == ParamModel::STATUS_TO_SUPPLEMENTED) {
                if ($params_payload_arr['statement'] == '') {
                    return json(['code'=>201, 'message'=>"请输入补充说明"]);
                } else {
                    if (strlen($params_payload_arr['statement']) > 300)
                    return json(['code'=>201, 'message'=>"超出字数限制"]);
                }
            }

            $data['photo_files'] = $params_payload_arr['photo_files'];
            $result = CertificationModel::onSiteVerify($data);
            if ($result && $data['status'] == ParamModel::STATUS_COMPLETED) {
                return json(['code'=>200, 'message'=>"核查完成"]);
            } elseif ($result && $data['status'] == ParamModel::STATUS_TO_SUPPLEMENTED) {
                return json(['code'=>200, 'message'=>"已退回补充材料"]);
            } else {
                return json(['code'=>404, 'message'=>"操作失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"操作失败"]);
        }
    }

    /**
     * issueCertificate 派发证书
     *
     * @return void
     */
    public function issueCertificate() {
        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $data['userid'] = request()->userInfo['userid'];
            $data['status'] = ParamModel::STATUS_DISTRIBUTED;
            $data['id'] = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            if ($data['id'] == '' || $data['id'] == null || $data['id'] == 0) {
                return json(['code'=>201, 'message'=>"参数id错误"]);
            }

            $data['report_files'] = $params_payload_arr['report_files'];
            $data['certificate_files'] = $params_payload_arr['certificate_files'];
            $result = CertificationModel::issueCertificate($data);

            if ($result) {
                return json(['code'=>200, 'message'=>"派发证书成功"]);
            } else {
                return json(['code'=>404, 'message'=>"证书派发失败"]);
            }

        } else {
            return json(['code'=>404, 'message'=>"操作失败"]);
        }
    }

    /**
     * addform 添加认证表单
     *
     * @return void
     */
    public function addform() {

        if (request()->isPost() && $this->validateForm($type = 2) === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data['data_stage'] = $params_payload_arr['data_stage'];
            $data['data_stage_code'] = $params_payload_arr['data_stage_code'];
            $data['category'] = $params_payload_arr['category'];
            $data['category_code'] = $params_payload_arr['category_code'];
            $data['attestation_id'] = $params_payload_arr['attestation_id'];
            $attestation_data = CertificationModel::getAttestation($data['attestation_id']);
            $data['organization_id'] = $attestation_data['main_organization_id'];
            $material = $params_payload_arr['material'];

            $data['name'] = $material['name'];
            $data['number'] = $material['number'];
            $data['unit_type'] = $material['unit_type'];
            $data['unit'] = $material['unit'];
            $data['source'] = $material['source'];
            $data['files'] = $params_payload_arr['files'];
            $data['factor_id'] = $params_payload_arr['factor_id'];
            $data['content'] = $params_payload_arr['content'];
            $data['compareDataList'] = $params_payload_arr['compareDataList'];
            $data['transport'] = isset($params_payload_arr['transport']) ? $params_payload_arr['transport'] : "";
            $data['create_time'] = date('Y_m-d H:i:s');
            $data['create_by'] = request()->userInfo['userid'];
            $data['data_from'] = ParamModel::DATA_FROM_COOPERATE;
            //因子数据
            $data['factor_id'] = $params_payload_arr['factor']['id'];
            $data['factor_title'] = $params_payload_arr['factor']['title'];
            $data['factor_denominator'] = $params_payload_arr['factor']['denominator'];
            $data['factor_value'] = $params_payload_arr['factor']['factor_value'];
            $data['factor_mechanism'] = $params_payload_arr['factor']['mechanism'];
            $data['factor_molecule'] = $params_payload_arr['factor']['molecule'];
            $data['factor_year'] = $params_payload_arr['factor']['year'];

            foreach ($data['compareDataList'] as $v) {
                $return = $this->validateAddForm($v);
                if ($return === true) {
                    continue;
                } else {
                    $datasmg['code'] = 404;
                    $datasmg['message'] = $return;
                    return json($datasmg);
                }
            };

            if (in_array($data['category_code'], ParamModel::TRANSPORT)) {
                if (empty($data['transport'])) {
                    return json(['code'=>404, 'message'=>"运输物品不能为空"]);
                }
            }

            $add = CertificationModel::addform($data);
            if ($add) {
                return json(['code'=>200, 'message'=>"添加成功"]);
            } else {
                return json(['code'=>201, 'message'=>"添加失败或数据已存在"]);
            }

        } else {
            return json(['code'=>404, 'message'=>$this->validateForm($type = 2)]);
        }
    }

    /**
     * editform 编辑认证表单
     *
     * @return void
     */
    public function editform() {

        if (request()->isPost() && $this->validateForm($type = 3) === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $data['id'] = $params_payload_arr['id'];
            $data['attestation_id'] = $params_payload_arr['attestation_id'];
            $data['data_stage'] = $params_payload_arr['data_stage'];
            $data['data_stage_code'] = $params_payload_arr['data_stage_code'];
            $data['category'] = $params_payload_arr['category'];
            $data['category_code'] = $params_payload_arr['category_code'];
            $material = $params_payload_arr['material'];
            $attestation_data = CertificationModel::getAttestation($data['attestation_id']);
            $data['organization_id'] = $attestation_data['main_organization_id'];

            $data['name'] = $material['name'];
            $data['number'] = $material['number'];
            $data['unit_type'] = $material['unit_type'];
            $data['unit'] = $material['unit'];

            $data['source'] = $material['source'];
            $data['files'] = $params_payload_arr['files'];
            $data['content'] = $params_payload_arr['content'];
            $data['compareDataList'] = $params_payload_arr['compareDataList'];
            $data['transport'] = isset($params_payload_arr['transport']) ? $params_payload_arr['transport'] : "";
            $data['del_transport'] = isset($params_payload_arr['del_transport']) ? $params_payload_arr['del_transport'] : "";
            $data['del_compareDataList'] = isset($params_payload_arr['del_compareDataList']) ? $params_payload_arr['del_compareDataList'] : "";
            $data['modify_time'] = date('Y_m-d H:i:s');
            $data['modify_by'] = request()->userInfo['userid'];
            $data['data_from'] = ParamModel::DATA_FROM_COOPERATE;
            $factory_data_list = $params_payload_arr['factory_data_list'];
            $data['factor_id'] = "";
            if ($factory_data_list === NULL) {
                $data['factor_id'] = $params_payload_arr['factor_id'];
                if (empty($params_payload_arr['factor']['id'])) {
                    return json(['code'=>404, 'message'=>"缺少因子id"]);
                }
                //因子数据
                $data['factor_id'] = $params_payload_arr['factor']['id'];
                $data['factor_title'] = $params_payload_arr['factor']['title'];
                $data['factor_denominator'] = $params_payload_arr['factor']['denominator'];
                $data['factor_value'] = $params_payload_arr['factor']['factor_value'];
                $data['factor_mechanism'] = $params_payload_arr['factor']['mechanism'];
                $data['factor_molecule'] = $params_payload_arr['factor']['molecule'];
                $data['factor_year'] = $params_payload_arr['factor']['year'];
            }


            foreach ($data['compareDataList'] as $v) {
                $return = $this->validateAddForm($v);
                if ($return === true) {
                    continue;
                } else {
                    $datasmg['code'] = 404;
                    $datasmg['message'] = $return;
                    return json($datasmg);
                }
            };

            if (in_array($data['category_code'], ParamModel::TRANSPORT)) {
                if (empty($data['transport'])) {
                    return json(['code'=>404, 'message'=>"运输物品不能为空"]);
                }
            }

            $edit = CertificationModel::editform($data);
            if ($edit) {
                return json(['code' => 200, 'message' => "编辑成功"]);
            } else {
                return json(['code' => 201, 'message' => "编辑失败或数据已存在"]);
            }

        } else {
            return json(['code' => 404, 'message' => $this->validateForm($type = 3)]);
        }
    }


    /**
     * delform 删除单个表单
     *
     * @return void
     */
    public function delform() {
        if (request()->isPost()) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);

            $id = isset($params_payload_arr['id']) ? $params_payload_arr['id'] : '';
            $category_code = isset($params_payload_arr['category_code']) ? $params_payload_arr['category_code'] : '';
            if ($id == '' || $id == null || $id == 0) {
                return json(['code' => 201, 'message' => "参数id错误"]);
            }
            $data['id'] = $id;
            $data['category_code'] = $category_code;
            $del = CertificationModel::delform($data);

            if ($del) {
                return json(['code' => 200, 'message' => "操作成功"]);
            } else {
                return json(['code' => 201, 'message' => "操作失败"]);
            }
        } else {
            return json(['code' => 404, 'message' => "请求方法错误"]);
        }
    }

    /**
     * upload 上传文件
     * 
     * @param $request
	 * @return void
     */
    public function upload(){
        $file = $this->request->file();

        try{
            // 验证文件大小格式
            if (isset($_POST['file_type']) && $_POST['file_type'] == 'image') {
                validate(['file' => 'fileSize:52428800|fileExt:png,jpg,txt'])->check($file);
            } else {
                validate(['file'=>'fileSize:52428800|fileExt:png,jpg,doc,docx,pdf,xls,xlsx'])->check($file);
            }

            $savename = \think\facade\Filesystem::disk('admin_cooperate_uploads')->putFile('admin_cooperate_uploads', $file['file']);
            $url = str_replace('\\', '/', $savename);
            $data['virtual_id'] = isset($_POST['virtual_id']) ? $_POST['virtual_id'] : NULL;
            $data['module'] = '合作端';
            $data['file_path'] = str_replace('admin_cooperate_uploads', '', $url);
            $data['source_name'] = $_FILES['file']['name'];
            $data['file_size'] = $_FILES['file']['size'];
            $data['create_by'] = request()->userInfo['userid'];
            $data['modify_by'] = request()->userInfo['userid'];
            $data['create_time'] = date('Y-m-d H:i:s');
            $data['modify_time'] = date('Y-m-d H:i:s');
            $data['id'] = guid();
            $add = CertificationModel::addFile($data);

            if ($add) {
                $datasmg['code'] = 200;
                $datasmg['id'] = $data['id'];
                $datasmg['name'] = $data['source_name'];
                $datasmg['msg'] = "上传成功"; // 前端要求， mseeage -> msg，不提示弹框
            } else {
                $datasmg['code'] = 404; // 前端要求，上传失败 404 -> 200，不提示弹框
                $datasmg['name'] = $data['source_name']; // 前端要求，上传失败返回文件名
                $datasmg['msg'] = "上传失败";
            }

            return json($datasmg);
        }catch(ValidateException $e){
            $datasmg['code'] = 200; // 前端要求，验证失败 404 -> 200，不提示弹框
            $datasmg['name'] = $_FILES['file']['name']; // 前端要求，验证失败返回文件名
            $datasmg['msg'] = $e->getError();

            return json($datasmg);
        }
    }

    /**
     * download zip文件下载
     * 
	 * @return void
     */
    public function download() {
        $ids = $_GET['id'];
        $id_arr = explode(',', $ids);
        $pathArr = [];
        foreach ($id_arr as $key => $value) {
            $list = CertificationModel::getFile($value);
            if ($list == NULL) {
                return json(['code'=>404, 'message'=>"文件不存在"]);
            }

            $file['path'] = dirname(dirname(dirname(dirname(__FILE__)))) . '/admin_cooperate_uploads' . $list['file_path'];
            $file['name'] = $list['source_name'];
            array_push($pathArr, $file);
        }

        $zipName = "下载文件.zip";
        self::makeZip($pathArr, $zipName);

	    // 输出压缩文件提供下载
	    header("Cache-Control: max-age=0");
	    header("Content-Description: File Transfer");
	    header('Content-disposition: attachment; filename=' . $zipName); // 文件名
	    header("Content-Type: application/zip"); // zip格式
	    header("Content-Transfer-Encoding: binary");
	    header('Content-Length: ' . filesize($zipName));
	    ob_clean();
	    flush();
	    readfile($zipName); // 输出文件;
	    unlink($zipName); // 删除压缩包临时文件
    }

    /**
     * filedownload 文件下载
     * 
     * @author wuyinghua
	 * @return void
     */
    public function filedownload() {
        $data['id'] = isset($_GET['id']) ? $_GET['id'] : '';
        $data['download_type'] = isset($_GET['download_type']) ? $_GET['download_type'] : '';
        $list = CertificationModel::getFile($data['id']);
        if ($list == NULL) {
            header('Content-Disposition: attachment;productfootnofile=productfootnofile');
            return json(['code'=>201, 'message'=>"文件不存在"]);
        }

        // 认证服务的文件
        if ($data['download_type'] == ParamModel::CERTIFICATION_TYPE) {
            $filePath = dirname(dirname(dirname(dirname(__FILE__)))) . '/admin_cooperate_uploads' . $list['file_path'];
            $fileName = $list['source_name'];

        // 产品碳足迹的文件下载 todo 测试服务器写的是9999
        } else {
            $filePath = dirname(dirname(dirname(dirname(__FILE__)))) . '/jayee-api-9999/public' . $list['file_path'];
            $fileName = $list['source_name'];
        }

        // 文件不存在，header返回前端标记productfootnofile
        if(!is_file($filePath)){
            header('Content-Disposition: attachment;productfootnofile=productfootnofile');
            return json(['code'=>201, 'message'=>"文件不存在"]);
        }

        return download($filePath, rawurlencode($fileName))->expire(300);
    }

    /**
     * see 查看产品核算详情
     * 
	 * @return void
     */
    public function see() {
        $id = $_GET['id'];
        $transport_ids = [8,12,14,16,18];

        $list = CertificationModel::getCalculate($id);
        $list_stage = explode(',', $list['stage']);
        $list_all = CertificationModel::seeCalculateData($id);

        $array = array();
        foreach ($list_stage as $list_key => $list_value) {
            $name = CertificationModel::getStageName($list_key + 1);
            foreach ($list_all as $value) {
                if ($value['data_stage'] == $list_value) {

                    if (in_array($value['category_id'], $transport_ids)) {
                        $value['unit_str'] = $value['unit_str'] . 'km';
                    }

                    $array[$list_key]['data_stage'] = $name['name'];
                    $array[$list_key]['datas'][] = $value;
                }
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = $array;
        $data['data']['product_name'] = $list['product_name'];
        $data['data']['product_no'] = $list['product_no'];
        $data['data']['product_spec'] = $list['product_spec'];
        $data['data']['week'] = $list['week'];
        $data['data']['number'] = $list['number'];
        $data['data']['coefficient'] = $list['coefficient'];
        $data['data']['remarks'] = $list['remarks'];
        $data['data']['unit'] = $list['unit'];
        $data['data']['unit_str'] = $list['unit_str'];

        $array = array();
        $product = CertificationModel::getDataById($list['product_id']);

        if ($product['files'] != NULL) {
            foreach (explode(',', $product['files']) as $file_key => $file_value) {
                $array[$file_key] = CertificationModel::getCalculateFiles($file_value);
            }
        }

        $data['data']['files'] = $array;
        $data['data']['total_emissions'] = $list['emissions'];

        return json($data);
    }

    /**
     * makeZip
     * 
	 * @return void
     */
    public function makeZip($pathArr, $zipName) {
        $zip = new \ZipArchive();
        if($zip->open($zipName, \ZipArchive::CREATE|\ZipArchive::OVERWRITE)) {
            foreach($pathArr as $file){
                if(!file_exists($file['path'])){
                    continue;
                }

                // 向压缩包中添加文件
                $zip->addFile($file['path'], $file['name']);
            }

            $zip->close();
            return ['code'=>200, 'msg'=>"创建成功", 'path'=>$zipName];
        }else{
            return ['code'=>404, 'msg'=>'创建失败'];
        }
    }

    /**
     * getCity
     * 
	 * @return void
     */
    public function getCity() {
        $citys = CertificationModel::getCitys();

        $city_tree = $this->getTree($citys, 0, 3);
        $return['code'] = 200;
        $return['list'] = $city_tree;
        return json($return);
    }

    /**
     * getTree
     * 
	 * @return void
     */
    public static function getTree($data, $pid, $level) {
        $tree = [];
        foreach ($data as $k => $v) {
            $level_value = isset($v['level']) ? $v['level'] : '';
            if ($level_value == $level) {
                break;
            }
            if ($v['pid'] == $pid) {
                $v['children'] = self::getTree($data, $v['id'], $level);
                $tree[] = $v;
                unset($data[$k]);
            }
        }

        return $tree;
    }

    /**
     * unitsee 单位数组
     * 
	 * @return void
     */
    public function unitsee() {
        $type = isset($_GET['type']) ? $_GET['type'] : '';
        $list = CertificationModel::getUnit($type)->toArray();

        $array = array();
        $dictionaries = array();

        foreach ($list as $key => $value) {
            if (!in_array($value['type_id'], $array)) {
                array_push($array, $value['type_id']);
                $dictionaries[$key]['id'] = $value['type_id'];
                $dictionaries[$key]['unit_type'] = $value['type'];
                $dictionaries[$key]['units'] = $this->initDatas($list, $value['type_id']);
            }
        }

        $data['code'] = 200;
        $data['data']['list'] = array_values($dictionaries);

        return json($data);
    }

    /**
     * factorSee 查看碳因子库详情
     *
     * @return void
     */
    public function factorSee() {
        $id = $_GET['id'];
        $list = CertificationModel::getFactor($id);

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

    /**
     * shotFactorSee 查看碳因子库快照详情
     *
     * @return void
     */
    public function shotFactorSee() {
        $id = $_GET['id'];
        $list = CertificationModel::getShotFactor($id);

        $data['code'] = 200;
        $data['data']['list'] = $list;

        return json($data);
    }

    /**
     * factor 排放因子列表
     * 
     * @author wuyinghua
	 * @return void
     */
    public function factor() {
        $page_size = isset($_GET['pageSize']) ? $_GET['pageSize'] : '';
        $page_index = isset($_GET['pageIndex']) ? $_GET['pageIndex'] : '';

        // 过滤条件：排放因子名称、发布国家/组织、发布年份
        $filters = [
            'filter_factor_source' => isset($_GET['filterFactorSource']) ? $_GET['filterFactorSource'] : '',
            'filter_factor_name' => isset($_GET['filterFactorName']) ? $_GET['filterFactorName'] : '',
            'filter_country' => isset($_GET['filterCountry']) ? $_GET['filterCountry'] : '',
            'filter_year' => isset($_GET['filterYear']) ? $_GET['filterYear'] : ''
        ];

        $list = CertificationModel::getFactors($page_size, $page_index, $filters)->toArray();
        $countrys = CertificationModel::getCountrys();
        $years = CertificationModel::getYears();

        $data['code'] = 200;
        $data['data']['list'] = $list['data'];
        $data['data']['countrys'] = $countrys;
        $data['data']['years'] = $years;
        $data['data']['total'] = $list['total'];

        return json($data);
    }

    /**
     * initDatas
     * 
     * @param $list
     * @param $type_id
	 * @return $array
     */
    private function initDatas($list, $type_id = '') {
        foreach ($list as $value) {
            if ($type_id == $value['type_id']) {
                $array[] = [
                    'unit_id'             => $value['id'],
                    'name'                => $value['name'],
                    'conversion_ratio'    => $value['conversion_ratio']
                ];
            }
        }

        return $array;
    }

    /**
     * validateForm 验证
     *
     * @return void
     */
    protected function validateForm($type)
    {
        $data = request()->param();
        try {
            if ($type == 1) {
                validate(CertificationValidate::class)->check($data);
            } elseif ($type == 2) {
                validate(CertificationAddFormValidate::class)->check($data);
            } elseif ($type == 3) {
                validate(CertificationEditFormValidate::class)->check($data);
            }
            return true;
        } catch (\think\exception\ValidateException $e) {

            return $e->getError();
        }
    }

    /**
     * validateAddForm 验证
     *
     * @return void
     */
    protected function validateAddForm($data) {

        try {
            validate(AttestationOriginalAddValidate::class)->check($data);
            return true;
        } catch (\think\exception\ValidateException $e) {

            return $e->getError();
        }
    }
}
