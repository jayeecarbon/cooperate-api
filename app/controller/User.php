<?php
declare (strict_types = 1);

namespace app\controller;

use app\BaseController;
use app\model\UserModel;
use app\validate\UserValidate;
use think\exception\ValidateException;

class User extends BaseController {

    /**
     * 登录
     *
     * @return \think\Response
     */
    public function login() {

        if ($this->request->isPost() && $this->validateForm() === true) {
            $params_payload = $this->req_payload;
            $params_payload_arr = json_decode($params_payload, true);
            $username = $params_payload_arr['username'];
            $password = md5($params_payload_arr['password'] . 'jieyitan');

            $user_data = UserModel::findUser($username);

            if (!$user_data){
                return json(['code'=>201,'message'=>'用户名错误']);
            }

            if($user_data['state'] != UserModel::STATE_TYPE_YES)
                return json(['code'=>201,'message'=>'该账号已停用']);

            if($password === $user_data['password']) {
                $user_data['last_time'] = date('Y-m-d H:i:s');
                $user_data['lastip'] = $this->request->ip();
                $user_data['login_times'] = $user_data['login_times'] + 1;
                
                $userinfo = ['userid'=>$user_data['id']];
                $user_data['token'] = signToken($userinfo);
                UserModel::updateUser($user_data);
             
                return json(['code' => 200, 'token' => $user_data['token'], 'username' => $username]);
            }else{
                return json(['code'=>201,'message'=>'密码错误']);
            }
        } else {
            return json(['code'=>404, 'message'=>$this->validateForm()]);
        }
    }

    /**
     * 登出
     *
     * @return \think\Response
     */
    public function loginout() {

        return json(['code' => 200]);
    }

    /**
     * validateForm 验证
     * 
	 * @return void
     */
    protected function validateForm() {
        $data = request()->param();

        try {
            validate(UserValidate::class)->check($data);

            return  true;
        } catch (ValidateException $e) {
            
            return $e->getError();
        }
    }
}
