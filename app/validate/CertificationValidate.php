<?php
declare (strict_types = 1);

namespace app\validate;

use think\Validate;

class CertificationValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名' =>  ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'verify_name'            => 'require|length:2,20',
        'verify_mobile'          => 'mobile',
        'retinue'                => 'require|float',
        'verify_start_time'      => 'require',
        'verify_end_time'        => 'require',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名' =>  '错误信息'
     *
     * @var array
     */
    protected $message = [
        'verify_name.require'          => '核查人员姓名不能为空',
        'verify_name.length'           => '核查人员姓名长度需在2-20个字符之间',
        'verify_mobile.mobile'         => '核查人员电话无效',
        'retinue.require'              => '随行人数不能为空',
        'retinue.float'                => '随行人数需要是数字',
        'verify_start_time.require'    => '核查起始时间不能为空',
        'verify_end_time.require'      => '核查结束时间不能为空',
    ];
}
