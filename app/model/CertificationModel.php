<?php
declare (strict_types = 1);

namespace app\model;

use think\facade\Db;

/**
 * @mixin \think\Model
 */
class CertificationModel extends Db {

    /**
     * getAttestations 查询产品碳足迹认证管理列表
     * 
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getAttestations($page_size, $page_index, $filters) {
        $where = [];
        if (isset($filters['filter_product_name']) && $filters['filter_product_name'] != '') {
            $where[] = array(['ja.product_name', 'like', '%' . trim($filters['filter_product_name']) . '%']);
        }

        if (isset($filters['filter_organization']) && $filters['filter_organization'] != '') {
            $where[] = array(['jo.name', 'like', '%' . trim($filters['filter_organization']) . '%']);
        }

        if (isset($filters['filter_attestation_type']) && $filters['filter_attestation_type'] != '') {
            $where[] = array(['ja.attestation_type', '=', trim($filters['filter_attestation_type'])]);
        }

        if (isset($filters['filter_status']) && $filters['filter_status'] != '') {
            $where[] = array(['ja.status', '=', trim($filters['filter_status'])]);
        }

        if (isset($filters['filter_time_start']) && isset($filters['filter_time_end']) && $filters['filter_time_start'] != '' && $filters['filter_time_end'] != '') {
            $where[] = array(['ja.create_time', '<=', $filters['filter_time_end']], ['ja.create_time', '>=', $filters['filter_time_start']]);
        }

        // 获取合作端显示的状态
        $status = implode(',', array_keys(ParamModel::STATUS_MAP));

        $list = Db::table('jy_attestation ja')
            ->field('ja.id, jo.name organization_name, ja.attestation_type, ja.product_name, ja.product_no, ja.status, ja.create_time, ja.modify_by, ja.report_files, ja.certificate_files, ja.data_from')
            ->leftJoin('jy_organization jo', 'ja.main_organization_id = jo.id')
            ->where($where)
            ->whereIn('ja.status', $status)
            ->order(['ja.modify_time'=>'desc', 'ja.create_time'=>'desc'])
            ->paginate(['list_rows' => $page_size, 'page' => $page_index])
            ->toArray();

        foreach ($list['data'] as $k => &$v) {
            if ($v['data_from'] == ParamModel::DATA_FROM_SAAS) {
                $find_name = Db::table('jy_user')
                    ->field('username')
                    ->where('id', $v['modify_by'])
                    ->find();

                $v['username'] = $find_name != NULL ? $find_name['username'] : '';

            } elseif ($v['data_from'] == ParamModel::DATA_FROM_ADMIN) {
                $find_name = Db::table('jy_mange_user')
                    ->field('username')
                    ->where('id', $v['modify_by'])
                    ->find();

                $v['username'] = $find_name != NULL ? $find_name['username'] : '';
            } else {
                $find_name = Db::table('jy_cooperate_user')
                    ->field('username')
                    ->where('id', $v['modify_by'])
                    ->find();

                $v['username'] = $find_name != NULL ? $find_name['username'] : '';
            }

            $list['data'][$k] = $v;
        }  

        return $list;
    }

    /**
     * getFiles 获取详情
     *
     * @param $files
     * @return $list
     */
    public static function getFiles($files) {
        $list = Db::table('jy_file jf')
            ->field('jf.id, jf.file_path, jf.source_name')
            ->whereIn('jf.id', $files)
            ->select();

        return $list;
    }

    /**
     * addFile 添加文件
     * 
     * @param $data
	 * @return $id
     */
    public static function addFile($data) {
        $add = Db::table('jy_file')->insert($data);

        return $add;
    }

    /**
     * getFile 查询文件
     * 
     * @param $id
	 * @return $list
     */
    public static function getFile($id) {
        $list = Db::table('jy_file jf')
            ->field('jf.id, jf.source_name, jf.file_path')
            ->where('id', $id)
            ->find();

        return $list;
    }

    /**
     * getOriginalById 根据认证ID查询表单
     * 
     * @param $id
	 * @return $list
     */
    public static function getOriginalById($id) {
        $list = Db::table('jy_attestation_original')
            ->where('attestation_id', (int)$id)
            ->select();

        return $list;
    }

    /**
     * getInfoById 获取详情
     *
     * @param $id
     * @return $list
     */
    public static function getInfoById($id) {
        $ip = 'http://47.122.18.241:81'; //todo
        $status_map = ParamModel::STATUS_MAP;
        $list = Db::table('jy_attestation ja')
            ->field('ja.id, ja.username, ja.mobile, ja.email, ja.attestation_type, ja.product_name,
            ja.product_no, ja.client_name, ja.client_address, ja.client_detail_address, ja.produce_name,
            ja.produce_address, ja.produce_detail_address, ja.company_name, ja.company_address, ja.company_detail_address,
            ja.data_time_border_start, ja.data_time_border_end, ja.system_boder, ja.functions, ja.files, ja.from, ja.product_id,
            ja.product_calculate_id, ja.status, ja.data_from')
            ->where('ja.id', (int)$id)
            ->find();

        if (isset($status_map[$list['status']])) {
            $list['status_name'] = $status_map[$list['status']];
        } else {
            $list['status_name'] = '';
        }

        $supplement = Db::table('jy_attestation_course')
            ->where(['attestation_id'=>$list['id'], 'title'=>'待现场核查', 'data_from'=>ParamModel::DATA_FROM_SAAS])
            ->find();

        $list['first_supplement'] = $supplement == NULL ? TRUE : FALSE;

        $product=[];
        if($list){
            if($list['files'] != NULL){
                $file = Db::table('jy_file jf')
                    ->field('jf.id, jf.file_path, jf.source_name')
                    ->whereIn('jf.id', $list['files'])
                    ->select()
                    ->toArray();

                if ($file) {

                    foreach ($file as $k => $v) {
                        $v['file_path'] = $ip . $v['file_path'];
                        $file[$k] = $v;
                    }
                }
                $list['files'] = $file;
            }

            //查询产品
            if($list['from'] == 1){
                $product = Db::table('jy_product_calculate jpc')
                    ->field('jpc.id product_calculate_id, 0 + CAST(jpc.number AS CHAR) number, jpc.unit_type, jpc.unit, jyu.name unit_str, jpc.scope, jpc.stage, jp.id, jpc.week_start, jpc.week_end, 
                    CONCAT_WS("-", jpc.week_start, jpc.week_end) week, jpc.remarks, jpc.state, DATE_FORMAT(jpc.modify_time, "%Y-%m-%d") modify_time, 0 + CAST(jpc.emissions AS CHAR) emissions,
                    0 + CAST(jpc.coefficient AS CHAR) coefficient, jp.id,jp.product_name, jp.product_no, jp.product_spec, jp.files')
                    ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
                    ->leftJoin('jy_unit jyu', 'jyu.id = jpc.unit')
                    ->where('jpc.id', (int)$list['product_calculate_id'])
                    ->find();
            }else{
                $product=[];
            }
        }
        $list['product'] = $product;

        // 认证流程 
        $process = Db::table('jy_attestation_course jac')
            ->field('jac.id, jac.title, jac.reject_cause, jac.statement, jac.verify_plan_file, jac.create_time, jac.verify_mobile, jac.retinue, jac.content, jac.verify_name, jac.verify_start_time, jac.verify_end_time,
            jac.photo_files, jac.report_files, jac.certificate_files, jac.data_from, jac.create_by')
            ->where('jac.attestation_id', $id)
            ->order('jac.id', 'desc')
            ->select()
            ->toArray();

        if ($process) {
            foreach ($process as $k => $v) {
                $v['photo_count'] = '';
                $v['big_pic_list'] = '';
                if ($v['photo_files'] != NULL) {
                    $photo_files = Db::table('jy_file')
                        ->field('id, file_path, source_name name')
                        ->whereIn('id', $v['photo_files'])
                        ->select()
                        ->toArray();

                    $big_file = [];
                    foreach ($photo_files as $pk => $pv) {
                        $pv['file_path'] = $ip . $pv['file_path'];
                        $big_file[] = $pv['file_path'];
                        $photo_files[$pk] = $pv;
                    }

                    $v['photo_files'] = $photo_files;
                    $v['photo_count'] = count($photo_files);
                    $v['big_pic_list'] = $big_file;
                }

                if ($v['report_files'] != NULL) {
                    $report_files = Db::table('jy_file')
                        ->field('id, file_path, source_name name')
                        ->whereIn('id', $v['report_files'])
                        ->select()
                        ->toArray();

                    $v['report_files'] = $report_files;
                }

                if ($v['certificate_files'] != NULL) {
                    $certificate_files = Db::table('jy_file')
                        ->field('id, file_path, source_name name')
                        ->whereIn('id', $v['certificate_files'])
                        ->select()
                        ->toArray();

                    $v['certificate_files'] = $certificate_files;
                }

                if ($v['data_from'] == ParamModel::DATA_FROM_SAAS) {
                    $find_name = Db::table('jy_user')
                        ->field('username')
                        ->where('id', $v['create_by'])
                        ->find();

                    $v['username'] = $find_name != NULL ? $find_name['username'] : '';

                } elseif ($v['data_from'] == ParamModel::DATA_FROM_ADMIN) {
                    $find_name = Db::table('jy_mange_user')
                        ->field('username')
                        ->where('id', $v['create_by'])
                        ->find();

                    $v['username'] = $find_name != NULL ? $find_name['username'] : '';
                } else {
                    $find_name = Db::table('jy_cooperate_user')
                        ->field('username')
                        ->where('id', $v['create_by'])
                        ->find();

                    $v['username'] = $find_name != NULL ? $find_name['username'] : '';
                }

                $process[$k] = $v;
            }
        }

        $list['process'] = $process;

        return $list;
    }

    /**
     * getdata 获取表单数据
     *
     * @return $del
     */
    public static function getdata($id) {
        $status_map = ParamModel::STATUS_MAP;
        $data =  Db::table('jy_attestation a')
            ->field('a.id attestation_id, a.product_name, a.from, a.system_boder, a.product_no,
            a.data_time_border_start, a.data_time_border_end, a.system_boder, a.product_id, a.product_calculate_id, a.status')
            ->where('id', (int)$id)
            ->find();

        if (isset($status_map[$data['status']])) {
            $data['status_name'] = $status_map[$data['status']];
        } else {
            $data['status_name'] = '';
        }

        $list = [];
        $return = [];
        $product_list = [];
        if ($data) {
            if ($data['system_boder'] == 1) {
                $list = Db::table('jy_data_stage')
                    ->field('id, pid, name')
                    ->where('pid', 0)
                    ->whereIn('name', ['原材料获取', '生产制造', '分销零售'])
                    ->order('id', 'asc')
                    ->select();

                foreach ($list as $k=>$v) {
                    $children = Db::table('jy_data_stage s')
                        ->field('s.name as data_stage, s.id, s.pid')
                        ->where('pid', $v['id'])
                        ->select()
                        ->toArray();

                    foreach ($children as $kk=>$vv) {
                        $vv['num'] = 0;
                        $vv['id'] = (string)$vv['id'];
                        $vv['pid'] = (string)$vv['pid'];
                        $children[$kk] = $vv;
                    }

                    $return[$k]['data_stage'] = $v['name'];
                    $return[$k]['id'] = (string)$v['id'];
                    $return[$k]['pid'] = (string)$v['pid'];
                    $return[$k]['num'] = 0;
                    $return[$k]['children'] = $children;
                }

            } elseif ($data['system_boder'] == 2) {
                $list = Db::table('jy_data_stage')
                    ->field('id,pid,name')
                    ->where('pid',0)
                    ->order('id','asc')
                    ->select();

                foreach ($list as $k => $v) {
                    $children = Db::table('jy_data_stage s')
                        ->field('s.name as data_stage, s.id, s.pid')
                        ->where('pid', $v['id'])
                        ->select()
                        ->toArray();

                    foreach ($children as $kk=>$vv) {
                        $vv['num'] = 0;
                        $vv['id'] = (string)$vv['id'];
                        $vv['pid'] = (string)$vv['pid'];
                        $children[$kk] = $vv;
                    }

                    $return[$k]['data_stage'] = $v['name'];
                    $return[$k]['id'] = (string)$v['id'];
                    $return[$k]['pid'] = (string)$v['pid'];
                    $return[$k]['num'] = 0;
                    $return[$k]['children'] = $children;
                }
            }

            foreach ($return as $v) {
                $one_data = [];
                $pro_one = Db::table('jy_product_data')
                    ->field('name, id')
                    ->where([
                        'product_id'           => $data['product_id'],
                        'product_calculate_id' => $data['product_calculate_id'],
                        'data_stage'           => $v['id']
                    ])
                    ->order('id','asc')
                    ->select()
                    ->toArray();

                $one_data['id'] = $v['id'];
                $one_data['data_stage'] = $v['data_stage'];
                $one_data['product_data'] = $pro_one;
                $product_list[] = $one_data;
            }
        }else{
            return false;
        }

        $form_list = Db::table('jy_attestation_original s')
            ->field('s.id,s.data_stage,s.data_stage_code,s.category,s.category_code,s.name,s.number,s.unit_type,
            s.unit,s.source,s.factor_id,s.content,s.err_status,s.err_msg,s.files,s.type,s.factor_type,s.factor_source,s.factor_from,
            s.factor_title,s.factor_value,s.factor_molecule,s.factor_source,s.factor_denominator,s.factor_year,s.factor_mechanism,s.factory_data_list')
            ->where('s.attestation_id', $id)
            ->order('s.id', 'desc')
            ->select()
            ->toArray();

        $soureList = [];
        if ($form_list) {
            foreach ($form_list as $k => $v) {
                $one_data = [];
                $material = [];
                $material['name'] = $v['name'];
                $material['number'] = $v['number'];
                $material['unit_type'] = $v['unit_type'];
                $material['unit'] = $v['unit'];
                $material['source'] = $v['source'];

                $one_data['id'] = $v['id'];
                $one_data['type'] = $v['type'];
                $one_data['content'] = $v['content'];
                $one_data['err_msg'] = $v['err_msg'];
                $one_data['err_status'] = $v['err_status'];
                
                $one_data['material'] = $material;
                $one_data['data_stage'] = $v['data_stage'];
                $one_data['data_stage_code'] = $v['data_stage_code'];
                $one_data['category'] = $v['category'];
                $one_data['category_code'] = $v['category_code'];
                $one_data['factor_from'] = $v['factor_from'];
                if ($v['factory_data_list']) {
                    // $v['factory_data_list'] = $v['factory_data_list'] == NULL ? '' : $v['factory_data_list'];
                    $one_data['factory_data_list'] = json_decode($v['factory_data_list']);
                } else {
                    $one_data['factory_data_list'] = NULL;
                }
                
                //文件数据
                $v_file = Db::table('jy_file s')
                    ->field('s.id, s.source_name name, s.file_path')
                    ->whereIn('id', $v['files'])
                    ->select()
                    ->toArray();
                $one_data['files'] = $v_file;

                //因子数据或者产品数据
                $factor_data=[];
                $factor_data['id'] = $v['factor_id'];
                $factor_data['title'] =$v['factor_title'];
                $factor_data['factor_value'] = round($v['factor_value'], 6);
                $factor_data['molecule'] = $v['factor_molecule'];
                $factor_data['denominator'] = $v['factor_denominator'];
                $factor_data['year'] = $v['factor_year'];
                $factor_data['mechanism'] = $v['factor_mechanism'];
                $one_data['factor'] = $factor_data;

                //对比数据
                $com = Db::table('jy_attestation_original_info s')
                    ->where('original_id', (int)$v['id'])
                    ->select()
                ->toArray();

                if ($com) {
                    foreach ($com as $kk=>$vv){
                       $files = Db::table('jy_file s')
                            ->field('s.id, s.source_name name, s.file_path')
                            ->whereIn('id', $vv['files'])
                            ->select()
                            ->toArray();
                       $vv['files'] = $files;
                       $com[$kk] = $vv;
                    }
                    $one_data['compareDataList'] = $com;
                }

                // 运输数据
                $trans = [];
                if (in_array($v['category_code'], ParamModel::TRANSPORT)) {
                    $trans = Db::table('jy_attestation_transport_info s')
                        ->where('original_id', $v['id'])
                        ->select()
                        ->toArray();

                    if ($trans) {
                        $one_data['transport'] = $trans;
                    }
                } else {
                    $one_data['transport'] = [];
                }
                $soureList[] = $one_data;
            }
        }

        $back = [];
        $data['soureList'] = $soureList;
        $data['product_list'] = $product_list;
        $back['leftmenu'] = $return;
        $back['topdata'] = $data;

        return $back;
    }

    /**
     * batchCompar 批量对比
     *
     * @param $data
     * @return void
     */
    public static function batchCompar($data) {
    
        Db::startTrans();
        try {
            foreach ($data['compar'] as $k => $v){
                Db::table('jy_attestation_original')->where('id', $v['id'])
                ->update(
                    [
                        'err_msg'     => $v['err_status'] == ParamModel::ERR_STATUS_YES ? '' : $v['err_msg'],
                        'err_status'  => $v['err_status'],
                        'data_from'   => ParamModel::DATA_FROM_COOPERATE,
                        'modify_by'   => (int)$data['userid'],
                        'modify_time' => date('Y-m-d H:i:s')
                    ]
                );
            }

            Db::commit();

            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return $e->getError();
        }
    }

    /**
     * Right 数据错误/数据正确
     *
     * @param $data
     * @return $update
     */
    public static function Right($data) {
        $update = Db::table('jy_attestation_original')->where('id', $data['id'])->update([
            'err_status'  => $data['err_status'],
            'err_msg'     => $data['err_msg'],
            'data_from'   => ParamModel::DATA_FROM_COOPERATE,
            'modify_by'   => (int)$data['userid'],
            'modify_time' => date('Y-m-d H:i:s')
        ]);

        return $update;
    }

    /**
     * onlineVerify 线上核查
     * @param $id
     * @param $status
     * @param $userid
     * @return void
     * @throws \think\db\exception\DbException
     */
    public static function onlineVerify($id, $status, $reject_cause, $userid) {
        Db::startTrans();
        try {
            // 认证状态变更
            Db::table('jy_attestation')->where('id', (int)$id)->update([
                'status'         => (int)$status,
                'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                'modify_by'      => (int)$userid,
                'modify_time'    => date('Y-m-d H:i:s')
            ]);

            // 通过线上核查
            if ($status == ParamModel::STATUS_TO_CHECK_ONSITE) {
                // 表单数据表状态变更
                Db::table('jy_attestation_original')->where('attestation_id', (int)$id)->update([
                    'type'           => ParamModel::TYPE_YES,
                    'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                    'modify_by'      => (int)$userid,
                    'modify_time'    => date('Y-m-d H:i:s')
                ]);

                // 将上一阶段 待线上核查（3）的待线上核查 更新为已通过线上核查（4）
                Db::table('jy_attestation_course')
                    ->where(['attestation_id' => (int)$id, 'title' => '待线上核查'])
                    ->update([
                        'status'         => (int)$status,
                        'title'          => '已通过线上核查',
                        'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                        'create_by'      => (int)$userid,
                        'create_time'    => date('Y-m-d H:i:s')
                    ]);

                // 插入一条记录待预约现场核查（4）
                Db::table('jy_attestation_course')
                    ->insert([
                        'attestation_id' => (int)$id,
                        'status'         => (int)$status,
                        'title'          => '待预约现场核查',
                        'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                        'create_by'      => (int)$userid,
                        'create_time'    => date('Y-m-d H:i:s')
                    ]);
            } else {
                // 表单数据表状态变更
                Db::table('jy_attestation_original')->where('attestation_id', (int)$id)->update([
                    'type'           => ParamModel::TYPE_YES,
                    'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                    'modify_by'      => (int)$userid,
                    'modify_time'    => date('Y-m-d H:i:s')
                ]);

                // 将上一阶段 待线上核查（3）的待线上核查 更新为未通过线上核查（5）
                Db::table('jy_attestation_course')
                    ->where(['attestation_id' => (int)$id, 'title' => '待线上核查'])
                    ->update([
                        'status'         => (int)$status,
                        'title'          => '未通过线上核查',
                        'reject_cause'   => $reject_cause,
                        'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                        'create_by'      => (int)$userid,
                        'create_time'    => date('Y-m-d H:i:s')
                    ]);
            }
            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * onSiteAppointmentVerify 预约现场核查
     * @param $data
     * @return void
     * @throws \think\db\exception\DbException
     */
    public static function onSiteAppointmentVerify($data) {
        Db::startTrans();
        try {
            // 认证状态变更
            Db::table('jy_attestation')->where('id', (int)$data['id'])->update([
                    'status'         => (int)$data['status'],
                    'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                    'modify_by'      => (int)$data['userid'],
                    'modify_time'    => date('Y-m-d H:i:s')
                ]);

            // 将上一阶段 待现场核查（4）的待预约现场核查 更新为已预约现场核查（6）
            Db::table('jy_attestation_course')
                ->where(['attestation_id' => (int)$data['id'], 'title' => '待预约现场核查'])
                ->update([
                    'status'            => (int)$data['status'],
                    'title'             => '已预约现场核查',
                    'verify_name'       => $data['verify_name'],
                    'verify_mobile'     => $data['verify_mobile'],
                    'retinue'           => $data['retinue'],
                    'content'           => $data['content'],
                    'verify_start_time' => $data['verify_start_time'],
                    'verify_end_time'   => $data['verify_end_time'],
                    'verify_plan_file'  => $data['verify_plan_file'],
                    'data_from'         => ParamModel::DATA_FROM_COOPERATE,
                    'create_by'         => (int)$data['userid'],
                    'create_time'       => date('Y-m-d H:i:s')
                ]);

            // 插入一条记录待预约现场核查（4）
            Db::table('jy_attestation_course')
                ->insert([
                    'attestation_id' => (int)$data['id'],
                    'status'         => (int)$data['status'],
                    'title'          => '待现场核查',
                    'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                    'create_by'      => (int)$data['userid'],
                    'create_time'    => date('Y-m-d H:i:s')
                ]);

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * onSiteVerify 现场核查
     * @param $data
     * @return void
     * @throws \think\db\exception\DbException
     */
    public static function onSiteVerify($data) {
        Db::startTrans();
        try {
            // 认证状态变更
            Db::table('jy_attestation')->where('id', (int)$data['id'])->update([
                'status'         => (int)$data['status'],
                'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                'modify_by'      => (int)$data['userid'],
                'modify_time'    => date('Y-m-d H:i:s')
            ]);

            // 通过线上核查
            if ($data['status'] == ParamModel::STATUS_COMPLETED) {
                // 将上一阶段 现场核查中（6）的待现场核查 更新为现场核查已通过（7）
                Db::table('jy_attestation_course')
                    ->where(['attestation_id' => (int)$data['id'], 'title' => '待现场核查'])
                    ->update([
                        'status'         => (int)$data['status'],
                        'title'          => '现场核查已通过',
                        'photo_files'    => $data['photo_files'],
                        'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                        'create_by'      => (int)$data['userid'],
                        'create_time'    => date('Y-m-d H:i:s')
                    ]);

                // 插入一条记录已完成认证，待派发证书（7）
                Db::table('jy_attestation_course')
                    ->insert([
                        'attestation_id' => (int)$data['id'],
                        'status'         => (int)$data['status'],
                        'title'          => '已完成认证，待派发证书',
                        'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                        'create_by'      => (int)$data['userid'],
                        'create_time'    => date('Y-m-d H:i:s')
                    ]);
            } else {
                // 将上一阶段 现场核查中（6）的待现场核查 更新为退回补充材料（8）
                Db::table('jy_attestation_course')
                    ->where(['attestation_id' => (int)$data['id'], 'title' => '待现场核查'])
                    ->update([
                        'status'         => (int)$data['status'],
                        'title'          => '退回补充材料',
                        'statement'      => $data['statement'],
                        'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                        'create_by'      => (int)$data['userid'],
                        'create_time'    => date('Y-m-d H:i:s')
                    ]);

                // 插入一条记录待补充材料（8）
                Db::table('jy_attestation_course')
                    ->insert([
                        'attestation_id' => (int)$data['id'],
                        'status'         => (int)$data['status'],
                        'title'          => '待补充材料',
                        'data_from'      => ParamModel::DATA_FROM_COOPERATE,
                        'create_by'      => (int)$data['userid'],
                        'create_time'    => date('Y-m-d H:i:s')
                    ]);
            }
            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * issueCertificate 派发证书
     * @param $data
     * @return void
     * @throws \think\db\exception\DbException
     */
    public static function issueCertificate($data) {
        Db::startTrans();
        try {
            // 认证状态变更、上传证书和报告
            Db::table('jy_attestation')->where('id', (int)$data['id'])->update([
                    'status'            => (int)$data['status'],
                    'data_from'         => ParamModel::DATA_FROM_COOPERATE,
                    'report_files'      => $data['report_files'],
                    'certificate_files' => $data['certificate_files'],
                    'modify_by'         => (int)$data['userid'],
                    'modify_time'       => date('Y-m-d H:i:s')
                ]);

            // 插入一条记录证书已派发（10）
            Db::table('jy_attestation_course')
                ->insert([
                    'attestation_id'    => (int)$data['id'],
                    'status'            => (int)$data['status'],
                    'title'             => '证书已派发',
                    'report_files'      => $data['report_files'],
                    'certificate_files' => $data['certificate_files'],
                    'data_from'         => ParamModel::DATA_FROM_COOPERATE,
                    'create_by'         => (int)$data['userid'],
                    'create_time'       => date('Y-m-d H:i:s')
                ]);
            
            // 订单更新为已完成状态
            // 认证状态变更、上传证书和报告
            Db::table('jy_attestation_order')->where('attestation_id', (int)$data['id'])->update([
                'status'            => (int)$data['status'],
                'data_from'         => ParamModel::DATA_FROM_COOPERATE,
                'modify_by'         => (int)$data['userid'],
                'modify_time'       => date('Y-m-d H:i:s')
            ]);

            Db::commit();

            return true;
        } catch (\Exception $e) {
            Db::rollback();

            return false;
        }
    }

    /**
     * getCalculate 获取产品核算
     * 
     * @param $id
	 * @return $list
     */
    public static function getCalculate($id) {
        $list = Db::table('jy_product_calculate jpc')
        ->field('jpc.id, jpc.product_id, 0 + CAST(jpc.number AS CHAR) number, jpc.unit_type, jpc.unit, ju.name unit_str, jpc.scope, jpc.stage, jpc.week_start, jpc.week_end, CONCAT_WS("-", jpc.week_start, jpc.week_end) week, jpc.remarks, jpc.state, DATE_FORMAT(jpc.modify_time, "%Y-%m-%d") modify_time, 0 + CAST(jpc.emissions AS CHAR) emissions, 0 + CAST(jpc.coefficient AS CHAR) coefficient')
        ->field('jp.product_name, jp.product_no, jp.product_spec')
        ->leftJoin('jy_unit ju', 'ju.id = jpc.unit')
        ->leftJoin('jy_product jp', 'jp.id = jpc.product_id')
        ->where('jpc.id', (int)$id)
        ->find();

        return $list;
    }

    /**
     * getStageName 获取产品核算阶段名称
     * 
     * @param $id
	 * @return $list
     */
    public static function getStageName($id) {
        $list = Db::table('jy_data_stage jds')->field('jds.name')->where('jds.id', (int)$id)->find();

        return $list;
    }

    /**
     * getDataById
     * 
     * @param $id
	 * @return $list
     */
    public static function getDataById($id)
    {
        $list = Db::table('jy_product jy')
            ->where('jy.id', (int)$id)
            ->find();
        return $list;
    }

    /**
     * seeCalculateData 查看产品核算详情
     * 
     * @param $id
	 * @return $list
     */
    public static function seeCalculateData($id) {
        $list = Db::table('jy_product_data jpd')
            ->field('jpd.data_stage, jpd.name, jpd.category category_id, jds.name category, 0 + CAST(jpd.number AS CHAR) number, jpd.unit, ju.name unit_str, jpd.match_type, jpd.match_id, 0 + CAST(jpd.emissions AS CHAR) emissions, 0 + CAST(jpd.coefficient AS CHAR) coefficient')
            ->leftJoin('jy_data_stage jds', 'jds.id = jpd.category')
            ->leftJoin('jy_unit ju', 'ju.id = jpd.unit')
            ->order(['jpd.modify_time'=>'desc', 'jpd.create_time'=>'desc'])
            ->where(['jpd.product_calculate_id' => (int)$id, 'jpd.is_del' => ParamModel::IS_DEL_NO])
            ->select();

        return $list;
    }

    /**
     * getCalculateFiles 获取产品核算文件
     *
     * @param $id
     * @return $list
     */
    public static function getCalculateFiles($id) {
        $list = Db::table('jy_file jf')
            ->field('jf.id, jf.source_name name, jf.file_path')
            ->where('jf.id', $id)
            ->order('jf.id', 'desc')
            ->find();

        return $list;
    }

    /**
     * getUnit 查询城市
     * 
	 * @return $list
     */
    public static function getCitys() {
        $list = Db::table('jy_city')
            ->field('id, pid, name, level')
            ->select();

        return $list;
    }

    /**
     * getUnit 查询单位
     * 
     * @param $type
	 * @return $list
     */
    public static function getUnit($type) {
        $where = array();

        if (!empty($type)) {
            $where[] = array(['jyu.type', '=', $type]);
        }

        $list = Db::table('jy_unit jyu')
            ->field('jyu.id, jyu.name, 0 + CAST(jyu.conversion_ratio AS CHAR) conversion_ratio, jyu.type_id, jut.unit_name type')
            ->leftJoin('jy_unit_type jut', 'jyu.type_id = jut.id')
            ->where($where)
            ->order(['jyu.type_id'=>'asc', 'jyu.id'=>'asc'])
            ->select();

        return $list;
    }

    /**
     * getFactor 获取因子详情
     *
     * @param $id
     * @return $del
     */
    public static function getFactor($id) {
        $list = Db::table('jy_factor jf')
            ->field('jf.id,jf.title,jf.factor_id,jf.model,jf.describtion,jf.describtion,jf.molecule
            ,jf.denominator,jf.year,jf.country,jf.region,jf.grade,jf.status,jf.factor_source,jf.create_by,
            jf.create_time,jf.file_name,jf.mechanism,jf.uncertainty,jf.factor_value')
            ->where('jf.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getShotFactor 获取因子快照详情
     *
     * @param $id
     * @return $del
     */
    public static function getShotFactor($id) {
        $list = Db::table('jy_factor_snapshot jf')
            ->field('jf.id,jf.title,jf.factor_id,jf.model,jf.describtion,jf.describtion,jf.molecule
            ,jf.denominator,jf.year,jf.country,jf.region,jf.grade,jf.status,jf.factor_source,jf.create_by,
            jf.create_time,jf.file_name,jf.mechanism,jf.uncertainty,jf.factor_value')
            ->where('jf.id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * getAttestation 获取因子详情
     *
     * @param $id
     * @return $list
     */
    public static function getAttestation($id) {
        $list = Db::table('jy_attestation')
            ->where('id', (int)$id)
            ->find();

        return $list;
    }

    /**
     * addform 添加认证表单
     *
     * @param $data
     * @return void
     */
    public static function addform($data) {
        $exists = Db::table('jy_attestation_original')
            ->where([
                    'data_stage_code' => $data['data_stage_code'],
                    'category_code'   => $data['category_code'],
                    'name'            => $data['name'],
                    'attestation_id'  => $data['attestation_id']
                ])
            ->find();
        if ($exists) {
            return false;
        }
        $material = $data['compareDataList'];
        $transport = $data['transport'];
        unset($data['compareDataList']);
        unset($data['transport']);
        unset($data['del_transport']);
        unset($data['del_compareDataList']);
        $snapshot_info =  Db::table('jy_factor')
            ->field('title,factor_id,language,title_language,model,describtion,mechanism,mechanism_short,grade,uncertainty,
            organization_id,year,country,region,file_name,factor_value,molecule,denominator,unit_type,factor_source,sort,status,f23,f24')
            ->where('id',$data['factor_id'])->find();

        Db::startTrans();
        try {

            $snapshot_info['organization_id'] = $data['organization_id'];
            $snapshot_info['create_time'] = date('Y-m-d H:i:s');
            $snapshot_info['create_by'] = $data['create_by'];
            $snapshot_id =  Db::table('jy_factor_snapshot')->insertGetId($snapshot_info);

            unset($data['organization_id']);
            $data['factor_id'] = $snapshot_id;
            $original_id = Db::table('jy_attestation_original')->insertGetId($data);

            foreach ($material as $k => $v) {
                unset($v['unitTypeOptions']);
                unset($v['valiFileListText']);
                unset($v['unitOptions']);
                unset($v['fileList']);
                $v['attestation_id'] = $data['attestation_id'];
                $v['original_id'] = $original_id;
                $v['create_time'] = date('Y-m-d H:i:s');
                $material[$k] = $v;
            }
            Db::table('jy_attestation_original_info')->insertAll($material);

            if (in_array($data['category_code'], ParamModel::TRANSPORT)) {
                //运输
                foreach ($transport as $k => $v) {
                    $v['attestation_id'] = $data['attestation_id'];
                    $v['original_id'] = $original_id;
                    $v['create_time'] = date('Y-m-d H:i:s');
                    $transport[$k] = $v;
                }
                Db::table('jy_attestation_transport_info')->insertAll($transport);
            }
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    /**
     * editform 编辑认证表单
     *
     * @param $data
     * @return void
     */
    public static function editform($data) {
        $exists = Db::table('jy_attestation_original')
            ->where([
                    'data_stage_code' => $data['data_stage_code'],
                    'category_code'   => $data['category_code'],
                    'name'            => $data['name'],
                    'attestation_id'  => $data['attestation_id']
                ])
            ->where('id', '<>', $data['id'])
            ->find();

        if ($exists) {
            return false;
        }
        $material = $data['compareDataList'];
        $transport = $data['transport'];
        $del_transport = $data['del_transport'];//运输
        $del_material = $data['del_compareDataList'];//对比
        unset($data['compareDataList']);
        unset($data['transport']);
        unset($data['del_compareDataList']);
        unset($data['del_transport']);
        if($data['factor_id']){
            $original_info =  Db::table('jy_attestation_original')->where('id',$data['id'])->find();
            if($original_info['factor_id']!=$data['factor_id']){
                $snapshot_info =  Db::table('jy_factor')
                    ->field('title,factor_id,language,title_language,model,describtion,mechanism,mechanism_short,grade,uncertainty,
             organization_id,year,country,region,file_name,factor_value,molecule,denominator,unit_type,factor_source,sort,status,f23,f24')
                    ->where('id',$data['factor_id'])->find();
            }
         }

        Db::startTrans();
        try {
            if($data['factor_id']){
                if($original_info['factor_id']!=$data['factor_id']){
                    $snapshot_info['organization_id'] = $data['organization_id'];
                    $snapshot_info['create_time'] = date('Y-m-d H:i:s');
                    $snapshot_info['create_by'] = $data['modify_by'];
                    $snapshot_id =  Db::table('jy_factor_snapshot')->insertGetId($snapshot_info);
                    $data['factor_id'] = $snapshot_id;
                }
            }
            unset($data['organization_id']);
            $update = Db::table('jy_attestation_original')->save($data);

            foreach ($material as $k => $v) {
                unset($v['unitTypeOptions']);
                unset($v['valiFileListText']);
                unset($v['unitOptions']);
                unset($v['fileList']);

                if (!empty($v['id'])) {
                    $v['modify_time'] = date('Y-m-d H:i:s', time());
                    Db::table('jy_attestation_original_info')->save($v);
                } else {
                    $v['attestation_id'] = $data['attestation_id'];
                    $v['original_id'] = $data['id'];
                    $v['create_time'] = date('Y-m-d H:i:s', time());
                    Db::table('jy_attestation_original_info')->insert($v);
                }
            }

            if (in_array($data['category_code'], ParamModel::TRANSPORT)) {
                //运输
                foreach ($transport as $k => $v) {

                    if (!empty($v['id'])) {
                        $v['create_time'] = date('Y-m-d H:i:s', time());
                        Db::table('jy_attestation_transport_info')->save($v);
                    } else {
                        unset($v['id']);
                        $v['attestation_id'] = $data['attestation_id'];
                        $v['original_id'] = $data['id'];
                        $v['create_time'] = date('Y-m-d H:i:s', time());
                        Db::table('jy_attestation_transport_info')->insert($v);
                    }
                }
            }
            //删除运输
            if (!empty($del_transport)) {
                Db::table('jy_attestation_transport_info')->whereIn('id', $del_transport)->delete();
            }
            //删除对比数据
            if (!empty($del_material)) {
                Db::table('jy_attestation_original_info')
                    ->whereIn('id', $del_material)->delete();
            }
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    /**
     * delform 删除表单数据
     *
     * @return $del
     */
    public static function delform($data) {
        Db::startTrans();
        try {
            Db::table('jy_attestation_original')->where('id', $data['id'])->delete();
            Db::table('jy_attestation_original_info')->where('original_id', $data['id'])->delete();
            // 删除运输
            if (in_array($data['category_code'], ParamModel::TRANSPORT)) {
                Db::table('jy_attestation_transport_info')->where('original_id', $data['id'])->delete();
            }
            Db::commit();
            return true;
        } catch (\think\exception\ValidateException $e) {
            Db::rollback();
            return false;
        }
    }

    /**
     * getFactors 查询排放因子
     * 
     * @param $page_ize
     * @param $page_index
     * @param $filters
	 * @return $list
     */
    public static function getFactors($page_size, $page_index, $filters) {
        $where = array();

        if ($filters['filter_factor_name']) {
            $where[] = array(['jf.title', 'like', '%' . trim($filters['filter_factor_name']) . '%']);
        }

        if ($filters['filter_country']) {
            $where[] = array(['jf.country', 'like', '%' . trim($filters['filter_country']) . '%']);
        }

        if ($filters['filter_year']) {
            $where[] = array(['jf.year', 'like', '%' . trim($filters['filter_year']) . '%']);
        }

        if (!empty($filters['filter_factor_source'])) {
            $where[] = array(['jf.factor_source', '=', $filters['filter_factor_source']]);
        }

        $list = Db::table('jy_factor jf')
            ->field('jf.id, jf.title, jf.model, 0 + CAST(jf.factor_value AS CHAR) factor_value, CONCAT_WS("/", jf.molecule, jf.denominator) unit, jf.year, jf.mechanism, jf.country, jf.factor_source')
            ->where($where)
            ->order('jf.id', 'asc')
            ->paginate(['list_rows'=>$page_size, 'page'=>$page_index]);

        return $list;
    }
    
    /**
     * getCountrys 发布国家/组织列表
     * 
	 * @return $list
     */
    public static function getCountrys() {
        $list = Db::table('jy_factor')->field('DISTINCT(country)')->select();

        return $list;
    }

    /**
     * getYears 发布年份
     * 
	 * @return $list
     */
    public static function getYears() {
        $list = Db::table('jy_factor')->field('DISTINCT(year)')->order('year', 'desc')->select();

        return $list;
    }
}
