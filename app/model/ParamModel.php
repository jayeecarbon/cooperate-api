<?php
declare (strict_types = 1);

namespace app\model;

use think\facade\Db;

/**
 * ParamModel
 */
class ParamModel extends Db {

    /**
     * 下载类型-认证
     */
    CONST CERTIFICATION_TYPE = 'certification';

    /**
     * 下载类型-认证zip
     */
    CONST CERTIFICATION_ZIP_TYPE = 'certification_zip';

    /**
     * 数据来源-SaaS端
     */
    CONST DATA_FROM_SAAS = 1;

    /**
     * 数据来源-后台端
     */
    CONST DATA_FROM_ADMIN = 2;

    /**
     * 数据来源-合作端
     */
    CONST DATA_FROM_COOPERATE = 3;

    /**
     * 数据正确/错误审核状态-未审核
     */
    CONST ERR_STATUS_UNAUDIT = 1;

    /**
     * 数据正确/错误审核状态-正确
     */
    CONST ERR_STATUS_YES = 2;

    /**
     * 数据正确/错误审核状态-错误
     */
    CONST ERR_STATUS_NO = 3;


    /**
     * 线上审核是否通过-通过
     */
    CONST TYPE_YES = 2;

    /**
     * 线上审核是否通过-未通过
     */
    CONST TYPE_NO = 1;

    /**
     * 状态-待付款
     */
    CONST STATUS_TO_PAY = 1;

    /**
     * 状态-待认证
     */
    CONST STATUS_TO_CERTIFIED = 2;

    /**
     * 状态-待线上核查
     */
    CONST STATUS_TO_CHECK_ONLINE = 3;

    /**
     * 状态-待现场核查
     */
    CONST STATUS_TO_CHECK_ONSITE = 4;

    /**
     * 状态-已驳回
     */
    CONST STATUS_REJECTED = 5;

    /**
     * 状态-现场核查中
     */
    CONST STATUS_ONSITE_CHECKING = 6;

    /**
     * 状态-认证完成
     */
    CONST STATUS_COMPLETED = 7;

    /**
     * 状态-待补充材料
     */
    CONST STATUS_TO_SUPPLEMENTED = 8;

    /**
     * 状态-认证失败
     */
    CONST STATUS_FAILED = 9;

    /**
     * 状态-证书已派发
     */
    CONST STATUS_DISTRIBUTED = 10;

    /**
     * 状态-服务终止
     */
    CONST STATUS_SERVICE_END = 11;

    /**
     * 状态-已取消
     */
    CONST STATUS_CANCELLED = 12;

    /**
     * 状态-已退款
     */
    CONST STATUS_REFUNDED = 13;

    /**
     * 认证类型-产品碳足迹
     */
    CONST AUTH_PRODUCT = 1;

    /**
     * 认证类型-企业碳核算
     */
    CONST AUTH_ENTERPRISE = 2;

    /**
     * 状态-下拉选择map
     */
    CONST STATUS_SELECT_MAP = [
        // ['id'=>self::STATUS_TO_PAY, 'name'=>'待付款'], // 合作端不显示
        // ['id'=>self::STATUS_TO_CERTIFIED, 'name'=>'待认证'], // 合作端不显示
        ['id'=>self::STATUS_TO_CHECK_ONLINE, 'name'=>'待线上核查'],
        ['id'=>self::STATUS_TO_CHECK_ONSITE, 'name'=>'待现场核查'],
        ['id'=>self::STATUS_REJECTED, 'name'=>'已驳回'],
        ['id'=>self::STATUS_ONSITE_CHECKING, 'name'=>'现场核查中'],
        ['id'=>self::STATUS_COMPLETED, 'name'=>'认证完成'],
        ['id'=>self::STATUS_TO_SUPPLEMENTED, 'name'=>'待补充材料'],
        // ['id'=>self::STATUS_FAILED, 'name'=>'认证失败'], //取消状态
        ['id'=>self::STATUS_DISTRIBUTED, 'name'=>'证书已派发'],
        // ['id'=>self::STATUS_SERVICE_END, 'name'=>'服务终止'], //取消状态
        // ['id'=>self::STATUS_CANCELLED, 'name'=>'已取消'], // 合作端不显示
        // ['id'=>self::STATUS_REFUNDED, 'name'=>'已退款'], //取消状态
    ];

    /**
     * 状态-下拉map
     */
    CONST STATUS_MAP = [
        // self::STATUS_TO_PAY =>'待付款', // 合作端不显示
        // self::STATUS_TO_CERTIFIED =>'待认证', // 合作端不显示
        self::STATUS_TO_CHECK_ONLINE =>'待线上核查',
        self::STATUS_TO_CHECK_ONSITE =>'待现场核查',
        self::STATUS_REJECTED =>'已驳回',
        self::STATUS_ONSITE_CHECKING =>'现场核查中',
        self::STATUS_COMPLETED =>'认证完成',
        self::STATUS_TO_SUPPLEMENTED =>'待补充材料',
        // self::STATUS_FAILED =>'认证失败', // 取消状态
        self::STATUS_DISTRIBUTED =>'证书已派发',
        // self::STATUS_SERVICE_END =>'服务终止', // 取消状态
        // self::STATUS_CANCELLED =>'已取消', // 合作端不显示
        // self::STATUS_REFUNDED =>'已退款', //取消状态
    ];

    /**
     * 认证类型-下拉选择map
     */
    CONST AUTH_SELECT_MAP = [
        ['id'=>self::AUTH_PRODUCT, 'name'=>'产品碳足迹'],
        ['id'=>self::AUTH_ENTERPRISE, 'name'=>'企业碳核算'],
    ];

    /**
     * 认证类型-下拉map
     */
    CONST AUTH_MAP = [
        self::AUTH_PRODUCT => '产品碳足迹',
        self::AUTH_ENTERPRISE => '企业碳核算',
    ];

    /**
     * 运输类-map
     */
    CONST TRANSPORT = [
        self::STAGE_TYPE_RAW_CATEGORY_TRANSPORT,
        self::STAGE_TYPE_MANUFACTURING_CATEGORY_TRANSPORT,
        self::STAGE_TYPE_DISTRIBUTION_TRANSPORT,
        self::STAGE_TYPE_USE_TRANSPORT,
        self::STAGE_TYPE_SCRAP_TRANSPORT
    ];

    /**
     * 审核流程名称-待线上核查
     */
    // CONST AUDIT_PROCESS;
    /**
     * 审核流程名称-待线上核查
     */


    /**
     * 是否删除-未删除
     */
    CONST IS_DEL_NO = 1;

    /**
     * 是否删除-已删除
     */
    CONST IS_DEL_YES = 2;

    /**
     * 状态-已完成
     */
    CONST STATE_COMPLETED = 1;

    /**
     * 状态-待审核
     */
    CONST STATE_REVIEW = 2;

    /**
     * 状态-编辑中
     */
    CONST STATE_EDIT = 3;

    /**
     * 阶段类型-原材料获取阶段
     */
    CONST STAGE_TYPE_RAW = 1;

    /**
     * 阶段类型-生产制造阶段
     */
    CONST STAGE_TYPE_MANUFACTURING = 2;

    /**
     * 阶段类型-分销零售阶段
     */
    CONST STAGE_TYPE_DISTRIBUTION= 3;

    /**
     * 阶段类型-使用阶段
     */
    CONST STAGE_TYPE_USE = 4;

    /**
     * 阶段类型-废弃处置阶段
     */
    CONST STAGE_TYPE_SCRAP = 5;

    /**
     * 阶段类型-原材料获取阶段-原材料类型
     */
    CONST STAGE_TYPE_RAW_CATEGORY_RAW = 6;

    /**
     * 阶段类型-原材料获取阶段-其他材料类型
     */
    CONST STAGE_TYPE_RAW_CATEGORY_OTHER = 7;

    /**
     * 阶段类型-原材料获取阶段-运输类型
     */
    CONST STAGE_TYPE_RAW_CATEGORY_TRANSPORT = 8;

    /**
     * 阶段类型-原材料获取阶段-能耗类型
     */
    CONST STAGE_TYPE_RAW_CATEGORY_ENERGY = 9;

    /**
     * 阶段类型-生产制造阶段-辅助材料类型
     */
    CONST STAGE_TYPE_MANUFACTURING_CATEGORY_AUXILIARY = 10;

    /**
     * 阶段类型-生产制造阶段-直接逸散类型
     */
    CONST STAGE_TYPE_MANUFACTURING_CATEGORY_ESCAPE= 11;

    /**
     * 阶段类型-生产制造阶段-运输类型
     */
    CONST STAGE_TYPE_MANUFACTURING_CATEGORY_TRANSPORT= 12;

    /**
     * 阶段类型-生产制造阶段-能耗类型
     */
    CONST STAGE_TYPE_MANUFACTURING_CATEGORY_ENERGY = 13;

    /**
     * 阶段类型-分销零售阶段-运输类型
     */
    CONST STAGE_TYPE_DISTRIBUTION_TRANSPORT= 14;

    /**
     * 阶段类型-分销零售阶段-能耗类型
     */
    CONST STAGE_TYPE_DISTRIBUTION_ENERGY= 15;

    /**
     * 阶段类型-使用阶段-运输类型
     */
    CONST STAGE_TYPE_USE_TRANSPORT = 16;

    /**
     * 阶段类型-使用阶段-能耗类型
     */
    CONST STAGE_TYPE_USE_ENERGY = 17;

    /**
     * 阶段类型-废弃处置阶段-运输类型
     */
    CONST STAGE_TYPE_SCRAP_TRANSPORT  = 18;

    /**
     * 阶段类型-废弃处置阶段-能耗类型
     */
    CONST STAGE_TYPE_SCRAP_ENERGY = 19;

    /**
     * 阶段类型-分销零售阶段-物料类型
     */
    CONST STAGE_TYPE_DISTRIBUTION_MATERIAL= 20;

    /**
     * 阶段类型-使用阶段-物料类型
     */
    CONST STAGE_TYPE_USE_MATERIAL = 21;

    /**
     * 阶段类型-废弃处置阶段-物料类型
     */
    CONST STAGE_TYPE_SCRAP_MATERIAL = 22;

    /**
     * 阶段类型-废弃处置阶段-直接逸散类型
     */
    CONST STAGE_TYPE_SCRAP_ESCAPE = 23;

    /**
     * 来源类型-自产
     */
    CONST SOURCE_TYPE_SELF = 1;

    /**
     * 来源类型-外采
     */
    CONST SOURCE_TYPE_EXTERNAL = 2;

    /**
     * 匹配类型-因子
     */
    CONST MATCH_TYPE_FACTOR = 1;

    /**
     * 匹配类型-产品
     */
    CONST MATCH_TYPE_PRODUCT = 2;


}
