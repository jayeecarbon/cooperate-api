<?php
 
declare(strict_types=1);
 
namespace app\middleware;
 
// 数据库
// use app\model\Admins;
use think\facade\Db;
use think\Exception;
use \think\facade\Request;
 
class CheckToken
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        try {
            $token = Request::header()['token'] ?? false;
            if(!$token)
                throw new Exception('Without Token', 201);
            $userinfo = checkToken($token);
            if($userinfo['code'] != 200)
                throw new Exception($userinfo['msg'], 202);

            $request->userInfo = $userinfo['data'];
        } catch (Exception $err){
            return json(['code'=>$err->getCode(), 'msg'=>$err->getMessage()]);
        }

        return $next($request);
    }
}