<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::get('think', function () {
    return 'hello,ThinkPHP6!';
});

Route::get('hello/:name', 'index/hello');

// 用户登录
Route::post('user/login', 'User/login');
// 产品碳足迹认证文件下载
Route::get('certificationdownload', 'certification/download');
// 文件下载路由
Route::get('filedownload', 'certification/filedownload');
// 单位管理数组路由
Route::get('unitsee', 'certification/unitsee');
// 获取城市列表
Route::get('getcity', 'certification/getcity');

Route::group(function () {
    // 用户登出
    Route::post('user/loginout', 'User/loginout');
    // 产品碳足迹认证管理列表
    Route::get('certification', 'certification/index');
    // 产品碳足迹认证信息详情
    Route::get('certificationinfo', 'certification/info');
    // 产品碳足迹认证数据及材料详情
    Route::get('certificationdatainfo', 'certification/datainfo');
    // 产品碳足迹认证线上核查
    Route::post('certificationonlineverify', 'certification/onlineverify');
    // 产品碳足迹认证预约现场核查
    Route::post('certificationonsiteappointmentverify', 'certification/onsiteappointmentverify');
    // 产品碳足迹认证现场核查
    Route::post('certificationonsiteverify', 'certification/onsiteverify');
    // 产品碳足迹认证派发证书
    Route::post('certificationissuecertificate', 'certification/issuecertificate');
    // 产品碳足迹认证文件上传
    Route::post('certificationupload', 'certification/upload');
    // 产品碳足迹认证表单数据正确/错误
    Route::post('certificationright', 'certification/right');
    // 产品碳足迹认证表单数据正确/错误批量对比
    Route::post('certificationbatchcompar', 'certification/batchcompar');
    // 产品核算详情路由
    Route::get('productcalculatesee', 'certification/see');
    // 因子选择
    Route::get('factor', 'certification/factor');
    // 因子详情
    Route::get('factorsee', 'certification/factorsee');
    // 因子快照详情
    Route::get('shotfactorsee', 'certification/shotfactorsee');
    //添加认证表单
    Route::post('addform', 'certification/addform');
    //编辑认证表单
    Route::post('editform', 'certification/editform');
    //删除认证表单
    Route::post('delform', 'certification/delform');
})->middleware(\app\middleware\CheckToken::class);